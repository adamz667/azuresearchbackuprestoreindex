﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using Azure;
using Azure.Search.Documents;
using Azure.Search.Documents.Indexes;
using Azure.Search.Documents.Models;
using Microsoft.Extensions.Configuration;

namespace AzureSearchBackupRestoreIndex
{
    static class Program
    {
        private static string _sourceSearchServiceName;
        private static string _sourceAdminKey;
        private static string _sourceIndexName;
        private static string _targetSearchServiceName;
        private static string _targetAdminKey;
        private static string _targetIndexName;
        private static string _backupDirectory;
        private static SearchIndexClient _sourceIndexClient;
        private static SearchClient _sourceSearchClient;
        private static SearchIndexClient _targetIndexClient;
        private static SearchClient _targetSearchClient;

        // name of field used to sort and filter search results
        private static readonly string FieldName = "DateAdded";
        
        // initial value used for 'lower than' filter
        // if filter field type is int as string then it should be surronded with semicolon e.g. @"'9999999'"
        // if filter field type is date just use DateTime in round-trip date/time pattern e.g. DateTime.UtcNow.ToString("O");
        private static string _startValueForLt = DateTime.UtcNow.ToString("O"); //@"'9999999'";

        // if field used to sort and filter is string set to false
        private static readonly bool IsFilterFieldInTypeOfDate = true;
        
        // JSON files will contain this many documents / file and can be up to 1000
        private static readonly int MaxBatchSize = 400;

        static void Main(string[] args)
        {
            //Get source and target search service info and index names from appsettings.json file
            //Set up source and target search service clients
            ConfigurationSetup();

            //Backup the source index
            Console.WriteLine("\nSTART INDEX BACKUP");
            BackupIndexAndDocuments();

            Console.WriteLine("Next step is INDEX RESTORE");
            Console.WriteLine("If you want to change scheme settings do it now and after that...");
            Console.WriteLine("...press any key to continue...");
            Console.ReadLine();
            
            //Recreate and import content to target index
            Console.WriteLine("\nSTART INDEX RESTORE");
            DeleteIndex();
            CreateTargetIndex();
            ImportFromJSON();
            Console.WriteLine("\r\n  Waiting 10 seconds for target to index content...");
            Console.WriteLine("  NOTE: For really large indexes it may take longer to index all content.\r\n");
            Thread.Sleep(10000);

            // Validate all content is in target index
            int sourceCount = GetCurrentDocCount(_sourceSearchClient);
            int targetCount = GetCurrentDocCount(_targetSearchClient);
            Console.WriteLine("\nSAFEGUARD CHECK: Source and target index counts should match");
            Console.WriteLine(" Source index contains {0} docs", sourceCount);
            Console.WriteLine(" Target index contains {0} docs\r\n", targetCount);

            Console.WriteLine("Press any key to continue...");
            Console.ReadLine();
        }

        static void ConfigurationSetup()
        {
            IConfigurationBuilder builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            IConfigurationRoot configuration = builder.Build();

            _sourceSearchServiceName = configuration["SourceSearchServiceName"];
            _sourceAdminKey = configuration["SourceAdminKey"];
            _sourceIndexName = configuration["SourceIndexName"];
            _targetSearchServiceName = configuration["TargetSearchServiceName"];
            _targetAdminKey = configuration["TargetAdminKey"];
            _targetIndexName = configuration["TargetIndexName"];
            _backupDirectory = configuration["BackupDirectory"];

            Console.WriteLine("CONFIGURATION:");
            Console.WriteLine("\n  Source service and index {0}, {1}", _sourceSearchServiceName, _sourceIndexName);
            Console.WriteLine("\n  Target service and index: {0}, {1}", _targetSearchServiceName, _targetIndexName);
            Console.WriteLine("\n  Backup directory: " + _backupDirectory);

            _sourceIndexClient = new SearchIndexClient(new Uri("https://" + _sourceSearchServiceName + ".search.windows.net"), new AzureKeyCredential(_sourceAdminKey));
            _sourceSearchClient = _sourceIndexClient.GetSearchClient(_sourceIndexName);

            _targetIndexClient = new SearchIndexClient(new Uri($"https://" + _targetSearchServiceName + ".search.windows.net"), new AzureKeyCredential(_targetAdminKey));
            _targetSearchClient = _targetIndexClient.GetSearchClient(_targetIndexName);
        }

        static void BackupIndexAndDocuments()
        {
            // Backup the index schema to the specified backup directory
            Console.WriteLine("\n  Backing up source index schema to {0}\r\n", _backupDirectory + "\\" + _sourceIndexName + ".schema");
            
            File.WriteAllText(_backupDirectory + "\\" + _sourceIndexName + ".schema", GetIndexSchema());
            
            // Extract the content to JSON files 
            var sourceDocCount = GetCurrentDocCount(_sourceSearchClient);
            WriteIndexDocuments(sourceDocCount);     // Output content from index to json files
        }

        static void WriteIndexDocuments(int currentDocCount)
        {
            int fileCounter = 0;
            for (int batch = 0; batch <= (currentDocCount / MaxBatchSize); batch++)
            {
                ++fileCounter;
                if ((fileCounter - 1) * MaxBatchSize < currentDocCount)
                {
                    Console.WriteLine("  Backing up source documents to {0} - (batch size = {1})", _backupDirectory + "\\" + _sourceIndexName + fileCounter + ".json", MaxBatchSize);
                    Console.WriteLine("StartValueForLt: " + _startValueForLt);
                    Console.WriteLine("batch number: " + batch);
                    Console.WriteLine("FileCounter: " + fileCounter);
                    ExportToJSON(_startValueForLt, _backupDirectory + "\\" + _sourceIndexName + fileCounter + ".json");
                }
            }
        }

        static void ExportToJSON(string startValue, string fileName)
        {
            // Extract all the documents from the selected index to JSON files in batches of MaxBatchSize / file
            string json = string.Empty;
            try
            {
                SearchOptions options = new SearchOptions()
                {
                    SearchMode = SearchMode.All,
                    Size = MaxBatchSize,
                    OrderBy = { $"{FieldName} desc" },
                    Filter = $"{FieldName} lt {startValue}"
                };

                var response = _sourceSearchClient.Search<SearchDocument>("*", options);
                var results = response.Value.GetResults();

                foreach (var doc in results)
                {
                    json += JsonSerializer.Serialize(doc.Document) + ",";
                    json = json.Replace("\"Latitude\":", "\"type\": \"Point\", \"coordinates\": [");
                    json = json.Replace("\"Longitude\":", "");
                    json = json.Replace(",\"IsEmpty\":false,\"Z\":null,\"M\":null,\"CoordinateSystem\":{\"EpsgId\":4326,\"Id\":\"4326\",\"Name\":\"WGS84\"}", "]");
                    json += "\r\n";
                }

                // Output the formatted content to a file
                json = json.Substring(0, json.Length - 3); // remove trailing comma
                File.WriteAllText(fileName, "{\"value\": [");
                File.AppendAllText(fileName, json);
                File.AppendAllText(fileName, "]}");
                Console.WriteLine("  Total documents: {0}", results.Count().ToString());
                
                // get last document and get value of field used to filter search results
                var lastOne = results.Last().Document;
                _startValueForLt = IsFilterFieldInTypeOfDate 
                    ? lastOne.First(x => x.Key == FieldName).Value.ToString() 
                    : $"'{lastOne.First(x => x.Key == FieldName).Value}'";
                Console.WriteLine("New startValue: " + _startValueForLt);
                return;
            }
            catch (Exception ex)
            {
                Console.WriteLine("startValue after error: " + startValue);
                Console.WriteLine("Error: {0}", ex.Message);
            }
        }

        static string GetIndexSchema()
        {
            // Extract the schema for this index
            // We use REST here because we can take the response as-is

            Uri serviceUri = new Uri("https://" + _sourceSearchServiceName + ".search.windows.net");
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("api-key", _sourceAdminKey);

            string schema = string.Empty;
            try
            {
                Uri uri = new Uri(serviceUri, "/indexes/" + _sourceIndexName);
                HttpResponseMessage response = AzureSearchHelper.SendSearchRequest(httpClient, HttpMethod.Get, uri);
                AzureSearchHelper.EnsureSuccessfulSearchResponse(response);
                schema = response.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
            }

            return schema;
        }

        private static bool DeleteIndex()
        {
            Console.WriteLine("\n  Delete target index {0} in {1} search service, if it exists", _targetIndexName, _targetSearchServiceName);
            // Delete the index if it exists
            try
            {
                _targetIndexClient.DeleteIndex(_targetIndexName);
            }
            catch (Exception ex)
            {
                Console.WriteLine("  Error deleting index: {0}\r\n", ex.Message);
                Console.WriteLine("  Did you remember to set your SearchServiceName and SearchServiceApiKey?\r\n");
                return false;
            }

            return true;
        }

        static void CreateTargetIndex()
        {
            Console.WriteLine("\n  Create target index {0} in {1} search service", _targetIndexName, _targetSearchServiceName);
            
            // Use the schema file to create a copy of this index
            // I like using REST here since I can just take the response as-is
            string json = File.ReadAllText(_backupDirectory + "\\" + _sourceIndexName + ".schema");

            // Do some cleaning of this file to change index name, etc
            json = "{" + json.Substring(json.IndexOf("\"name\""));
            int indexOfIndexName = json.IndexOf("\"", json.IndexOf("name\"") + 5) + 1;
            int indexOfEndOfIndexName = json.IndexOf("\"", indexOfIndexName);
            json = json.Substring(0, indexOfIndexName) + _targetIndexName + json.Substring(indexOfEndOfIndexName);

            Uri serviceUri = new Uri("https://" + _targetSearchServiceName + ".search.windows.net");
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("api-key", _targetAdminKey);

            try
            {
                Uri uri = new Uri(serviceUri, "/indexes");
                HttpResponseMessage response = AzureSearchHelper.SendSearchRequest(httpClient, HttpMethod.Post, uri, json);
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                Console.WriteLine("  Error: {0}", ex.Message);
            }
        }

        static int GetCurrentDocCount(SearchClient searchClient)
        {
            // Get the current doc count of the specified index
            try
            {
                SearchOptions options = new SearchOptions()
                {
                    SearchMode = SearchMode.All,
                    IncludeTotalCount = true
                };

                SearchResults<Dictionary<string, object>> response = searchClient.Search<Dictionary<string, object>>("*", options);
                return Convert.ToInt32(response.TotalCount);
            }
            catch (Exception ex)
            {
                Console.WriteLine("  Error: {0}", ex.Message);
            }

            return -1;
        }

        static void ImportFromJSON()
        {
            Console.WriteLine("\n  Upload index documents from saved JSON files");
            // Take JSON file and import this as-is to target index
            Uri serviceUri = new Uri("https://" + _targetSearchServiceName + ".search.windows.net");
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("api-key", _targetAdminKey);

            try
            {
                foreach (string fileName in Directory.GetFiles(_backupDirectory, _sourceIndexName + "*.json"))
                {
                    Console.WriteLine("  -Uploading documents from file {0}", fileName);
                    string json = File.ReadAllText(fileName);
                    Uri uri = new Uri(serviceUri, "/indexes/" + _targetIndexName + "/docs/index");
                    HttpResponseMessage response = AzureSearchHelper.SendSearchRequest(httpClient, HttpMethod.Post, uri, json);
                    response.EnsureSuccessStatusCode();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("  Error: {0}", ex.Message);
            }
        }
    }
}
